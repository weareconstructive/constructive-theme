import Router from './library/DomBasedRouter';

const router = new Router({
    common: {
        init() {
            if (document.body.classList.contains('admin-bar')) {
                document.documentElement.classList.add('admin-bar');
            }
        }
    },
    home: {
        init() {
            // alert('home page')
        }
    }
}).load();
