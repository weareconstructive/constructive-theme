// Import Stylesheets Here
import 'normalize.css';
import './styles/main.scss';

// Import Scripts Here
import './scripts/main.js';
