<?php

add_theme_support('menus');
// can pass arg for post type as well
add_theme_support('post-thumbnails');

add_theme_support('title-tag');

add_theme_support('html5', [
    'comment-list',
    'comment-form',
    'search-form',
    'gallery',
    'caption'
]);



    /* Enqueues */
function theme_enqueues()
{
    define('THEME_VERSION', '1.0');
    wp_enqueue_style('main-style', get_stylesheet_directory_uri() . '/dist/main.css', [], THEME_VERSION);
    wp_enqueue_script('main-script', get_stylesheet_directory_uri() . '/dist/main.js', [], THEME_VERSION, true);

    // wp_localize_script(
    //     'main-script',
    //     'wordpress_frontend',
    //     $wpData
    // );

}

add_action('wp_enqueue_scripts', 'theme_enqueues');