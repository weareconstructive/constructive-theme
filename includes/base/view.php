<?php

/**
 * Returns a view file (typically a row or component)
 */

function view($path, $args = [])
{
    if (!$path || $path == "") {
        return;
    }

    $realPath = realpath(dirname(__FILE__) . '/../../views/' . $path . '.php');

    if (!file_exists($realPath)) {
        throw new Exception("$realPath doesn't exist.");
        return;
    }

    /* Reserved argument keys are prefix and options
    /  Prefix, sets the prefix
    /  Options, sets the name of the options page if we are on one.

    /* Set an empty prefix in the case that there is none. */
    if (!isset($args['prefix'])) {
        $args['prefix'] = "";
    } else if ($args['prefix'][count($args['prefix']) - 2] !== "_") {
        $args['prefix'] = $args['prefix'] . "_";
    }

    /* If it isn't explicitly set, set options to false. */
    if (!isset($args['options'])) {
        $args['options'] = false;
    }

    /* Set any variables */
    if ($args && gettype($args) == 'array') {
        foreach ($args as $key => $value) {
            $$key = $value;
        }
    }

    // Set Up The Field and Get Field depending on context

    // if (get_row_layout() !== false || get_row() !== false) {

    //     if ($options) {

    //         $get = function ($field) use ($prefix) {
    //             return get_sub_field($prefix . $field, "options");
    //         };

    //         $the = function ($field) use ($prefix) {
    //             the_sub_field($prefix . $field, "options");
    //         };

    //         $rows = function ($field, $callback) use ($prefix) {
    //             while (have_rows($prefix . $field, "options")):
    //                 $get = function ($field) use ($prefix) {
    //                     return get_sub_field($field, "options");
    //                 };
    //                 $the = function ($field) use ($prefix) {
    //                     the_sub_field($field, "options");
    //                 };
    //                 the_row();
    //                 $callback($get, $the);
    //             endwhile;
    //         };

    //     } else {

    //         $get = function ($field) use ($prefix) {
    //             return get_sub_field($prefix . $field);
    //         };

    //         $the = function ($field) use ($prefix) {
    //             the_sub_field($prefix . $field);
    //         };

    //         $rows = function ($field, $callback) use ($prefix) {
    //             while (have_rows($prefix . $field)):
    //                 $get = function ($field) use ($prefix) {
    //                     return get_sub_field($field);
    //                 };
    //                 $the = function ($field) use ($prefix) {
    //                     the_sub_field($field);
    //                 };
    //                 the_row();
    //                 $callback($get, $the);
    //             endwhile;
    //         };

    //     }

    // } else {

    //     if ($options) {

    //         $get = function ($field) use ($prefix) {
    //             return get_field($prefix . $field, "options");
    //         };

    //         $the = function ($field) use ($prefix) {
    //             the_field($prefix . $field, "options");
    //         };

    //         $rows = function ($field, $callback) use ($prefix) {
    //             while (have_rows($prefix . $field, "options")):
    //                 $get = function ($field) use ($prefix) {
    //                     return get_sub_field($field, "options");
    //                 };
    //                 $the = function ($field) use ($prefix) {
    //                     the_sub_field($field, "options");
    //                 };
    //                 the_row();
    //                 $callback($get, $the);
    //             endwhile;
    //         };

    //     } else {

    //         $get = function ($field) use ($prefix) {
    //             return get_field($prefix . $field);
    //         };

    //         $the = function ($field) use ($prefix) {
    //             the_field($prefix . $field);
    //         };

    //         $rows = function ($field, $callback) use ($prefix) {
    //             while (have_rows($prefix . $field)):
    //                 $get = function ($field) use ($prefix) {
    //                     return get_sub_field($field);
    //                 };
    //                 $the = function ($field) use ($prefix) {
    //                     the_sub_field($field);
    //                 };
    //                 the_row();
    //                 $callback($get, $the);
    //             endwhile;
    //         };

    //     }

    // }

    include $realPath;

    /* Unset any variables passed for this partial */
    if ($args && gettype($args) == 'array') {
        foreach ($args as $key => $value) {
            unset($$key);
        }
    }
}