<?php if (have_posts()):
    while (have_posts()):
        the_post();?>
<article>
	<h1>
		<a href="<?= get_permalink(); ?>">
			<?php the_title();?>
		</a>
	</h1>
	<?php the_content();?>
</article>
<?php endwhile;endif;?>