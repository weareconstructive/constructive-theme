<?php use Theme\Utils;?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head();?>
</head>

<body <?=body_class();?>>

    <div class="wrap">
        <?php view('rows/header');?>
        <main>
            <?php include Utils\template_path();?>
        </main>
    </div>

    <?php view('rows/footer');?>
    <?php wp_footer();?>

</body>

</html>
