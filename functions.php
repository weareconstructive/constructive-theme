<?php

// require "vendor/autoload.php";

foreach (glob(__DIR__ . "/includes/base/*.php") as $filename) {
    include $filename;
}

foreach (glob(__DIR__ . "/includes/theme/*.php") as $filename) {
    include $filename;
}

include_once "includes/theme.php";