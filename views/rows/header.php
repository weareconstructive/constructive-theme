<header>
    <div class="container">
        <a href="<?= home_url(); ?>" class="site-title">
            <?= bloginfo('site_title'); ?>
        </a>
        <?php wp_nav_menu(['menu' => 'Primary']); ?>
    </div>
</header>